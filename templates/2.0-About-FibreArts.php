<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-1.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">About</span>
						<span class="subtitle">
							<span>Sed dictum sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
						<a href="#" class="button big blue">Register</a>	
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">
		
			<article>
			
				<div class="article-body ov-article">
		
					<div class="section-header">
						<h2 class="title">The Versatility of Fibre</h2>
					</div><!-- .section-header -->
					
					<p>
						Set in the sublime splendor of Gros Morne National Park, a UNESCO World Heritage Site, this international four-day conference will
						celebrate the versatility of fibre. Fibre Arts Newfoundland and Labrador will consist of: workshops from nationally and internationally
						renowned fibre instructors, artist talks, exhibitions, seminars, and tours. This event is bound to inspire and develop your own fibre art
						while leaving you in love with this place full of wild, pure aesthetic wonder.
					</p>
						 
					<p>
						Please note that this is a website to promote the workshops. All additional conference details: evening entertainment, transport,
						accommodation, price lists etc. will be provided in greater detail upon the launch of the registration website on January 27 2015.
					</p>
				
				</div><!-- .article-body -->
				
			</article>

		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="grid ov-blocks">
				<div class="col">
					<div class="item">
					
						<span class="ov-blocks-title">Fibre Arts NL Conferece</span>
						
						<span class="hex-separator">
							<span>&nbsp;</span>
						</span>
						
						<p>
							Claritas est etiam processus
							dynamicus, qui sequitur mutationem
							consuetudium lectorum.
						</p>
					
						<a href="#" class="button blue">More</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col">
					<div class="item">
					
						<span class="ov-blocks-title">Sponsors</span>
						
						<span class="hex-separator">
							<span>&nbsp;</span>
						</span>
						
						<p>
							Claritas est etiam processus
							dynamicus, qui sequitur mutationem
							consuetudium lectorum.
						</p>
					
						<a href="#" class="button blue">More</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col">
					<div class="item">
					
						<span class="ov-blocks-title">FAQs</span>
						
						<span class="hex-separator">
							<span>&nbsp;</span>
						</span>
						
						<p>
							Claritas est etiam processus
							dynamicus, qui sequitur mutationem
							consuetudium lectorum.
						</p>
					
						<a href="#" class="button blue">More</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>