<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-2.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">Basketry</span>
						<span class="subtitle">
							<span>Sed dictuClaritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.m sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	
	<section>
		<div class="sw">
		
			<div class="workshop-info workshop-info-header">
				<div class="workshop-info-dates">
					
					<div class="section-header">
						<h3 class="title">Workshop Dates</h3>
					</div><!-- .section-header -->
				
					<div class="ib">
						<div class="date-block">
							<span class="dow">Wednesday</span>
							<span class="date">14</span>
							<span class="month">October</span>
						</div>
						
						<a href="#" class="button blue sm block">Register</a>
					</div><!-- .ib -->
					
					<div class="ib">
						<div class="date-block">
							<span class="dow">Thursday</span>
							<span class="date">15</span>
							<span class="month">October</span>
						</div>
						
						<a href="#" class="button blue sm block">Register</a>
					</div><!-- .ib -->
				
				</div><!-- .workshop-info-dates -->
				
				<div class="workshop-info-instructor">
				
					<div class="section-header">
						<h3 class="title">Instructors</h3>
					</div><!-- .section-header -->
				
					<a href="#" class="ib">				
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/vanessa.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Vanessa Smith</span>
					</a>
					
				</div><!-- .workshop-info-instructor -->
				
			</div><!-- .workshop-info -->
			
			<div class="article-body ov-article">
				<p>
					Sed dictum sem ac hendrerit elementum. Maecenas aliquet ante id tortor bibendum egestas. In eu consectetur augue, ut rutrum dolor. 
					Fusce non sagittis ipsum. Integer vel vehicula sapien, sed dapibus eros. Ut id massa lacinia, vestibulum urna quis, condimentum mi. Aliquam erat volutpat. 
					Quisque convallis nunc sit amet eros dignissim, auctor aliquam nisl congue. Nullam id ornare nisi.
				</p>
			</div>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="section-header">
				<h2 class="title">Workshops</h2>
			</div><!-- .section-header -->
			
			
			<div class="workshop-info">
			
				<div class="workshop-info-instructor">
				
					<a href="#" class="ib">				
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/vanessa.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Eileen Murphy / Branching Out</span>
					</a>
					
					<a href="#" class="ib">				
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/vanessa.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Eileen Murphy / Branching Out</span>
					</a>
					
				</div><!-- .workshop-info-instructor -->
				
				<br />
			
				<div class="workshop-info-dates ib">
				
					<div class="ib">
						<div class="date-block">
							<span class="dow">Wednesday</span>
							<span class="date">14</span>
							<span class="month">October</span>
						</div>
						
						<a href="#" class="button blue sm block">Register</a>
					</div><!-- .ib -->
					
					<div class="ib">
						<div class="date-block">
							<span class="dow">Thursday</span>
							<span class="date">15</span>
							<span class="month">October</span>
						</div>
						
						<a href="#" class="button blue sm block">Register</a>
					</div><!-- .ib -->
					
					<div class="ib">
						<div class="date-block">
							<span class="dow">Friday</span>
							<span class="date">16</span>
							<span class="month">October</span>
						</div>
						
						<a href="#" class="button blue sm block">Register</a>
					</div><!-- .ib -->
					
					<br />
					<br />
					
					<a href="#" class="button blue block">VIew Gallery</a>
				
				</div><!-- .workshop-info-dates -->
				
				
			</div><!-- .workshop-info -->

			<div class="workshop-info">
			
				<div class="workshop-info-instructor">
				
					<a href="#" class="ib">				
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/vanessa.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Eileen Murphy / Branching Out</span>
					</a>
					
				</div><!-- .workshop-info-instructor -->
				
				<br />
			
				<div class="workshop-info-dates ib">
				
					<div class="ib">
						<div class="date-block">
							<span class="dow">Wednesday</span>
							<span class="date">14</span>
							<span class="month">October</span>
						</div>
						
						<a href="#" class="button blue sm block">Register</a>
					</div><!-- .ib -->
					
					<div class="ib">
						<div class="date-block">
							<span class="dow">Thursday</span>
							<span class="date">15</span>
							<span class="month">October</span>
						</div>
						
						<a href="#" class="button blue sm block">Register</a>
					</div><!-- .ib -->
					
					<br />
					<br />
					
					<a href="#" class="button blue block">VIew Gallery</a>
				
				</div><!-- .workshop-info-dates -->
				
				
			</div><!-- .workshop-info -->
			
			
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>