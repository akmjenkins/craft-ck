<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-1.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">Register</span>
						<span class="subtitle">
							<span>Sed dictum sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">
		
			<div class="section-header">
				<h2 class="title">Payment</h2>
			</div><!-- .section-header -->
			
			<div class="registration-wrapper">
				<div class="active-registrations">
				
					<div class="active-registration-item">
					
						<span class="t-fa fa-close delete-item">&nbsp;</span>
					
						<span class="active-registration-name">John Atkins</span>
						<span class="active-registration-total">$525.00</span>
					
					</div><!-- .active-registration-item -->
					
					<div class="active-registration-item">
					
						<span class="t-fa fa-close delete-item">&nbsp;</span>
					
						<span class="active-registration-name">John Atkins</span>
						<span class="active-registration-total">$525.00</span>
					
					</div><!-- .active-registration-item -->
					
				</div><!-- .active-registrations -->
				
				<div class="reg-section dark-bg cc-form-section">
				
					<h3>Total</h3>
				
					<div class="reg-totals">
					
						<div class="reg-total-item">
							<div class="reg-total-label">Subtotal</div>
							<div class="reg-total-price">$1050</div>
						</div><!-- .reg-total-item -->
						
						<div class="reg-total-item">
							<div class="reg-total-label">Tax (13%)</div>
							<div class="reg-total-price">$136.50</div>
						</div><!-- .reg-total-item -->
						
						<div class="reg-total-item">
							<div class="reg-total-label">Total</div>
							<div class="reg-total-price">$1186.50</div>
						</div><!-- .reg-total-item -->
						
					</div><!-- .reg-totals -->
				
					<form action="/" class="body-form full cc-form" novalidate>
						
						<div class="fieldset">
						
							<h3>Card Holder Information</h3>
							
							<span class="field-wrap">
								<input type="text" name="cc_fname" placeholder="First Name">
							</span>
							
							<span class="field-wrap">
								<input type="text" name="cc_lname" placeholder="Last Name">
							</span>
							
							<span class="field-wrap">
								<select name="cc_country">
									<option value="">-- Select Your Country --</option>
									<option value="CA">Canada</option>
									<option value="US">United States of America</option>
									<option value=""> --- </option>
								</select>
							</span>
							
							<br />
							
							<h3>Credit Card Information</h3>
							
							<label>Credit Card Number</label>
							<span class="field-wrap">
								<input type="tel" name="cc_number" placeholder="Credit Card Number">
							</span><!-- .field-wrap -->
							
							<label>Expiry Date</label>
							<div class="grid">
								<div class="col-2 col">
									<div class="item">
										<span class="field-wrap">
											<select name="cc_exp_mm">
												<option value="1" selected="selected">01 - January</option>
												<option value="2">02 - February</option>
												<option value="3">03 - March</option>
												<option value="4">04 - April</option>
												<option value="5">05 - May</option>
												<option value="6">06 - June</option>
												<option value="7">07 - July</option>
												<option value="8">08 - August</option>
												<option value="9">09 - September</option>
												<option value="10">10 - October</option>
												<option value="11">11 - November</option>
												<option value="12">12 - December</option>
											</select>
										</span><!-- .field-wrap -->
									</div>
								</div><!-- .col -->
								<div class="col-2 col">
									<div class="item">
										<span class="field-wrap">
											<select name="cc_exp_yy">
												<option value="2015">2015</option>
												<option value="2016" selected="selected">2016</option>
												<option value="2017">2017</option>
												<option value="2018">2018</option>
												<option value="2019">2019</option>
												<option value="2020">2020</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
												<option value="2023">2023</option>
												<option value="2024">2024</option>
												<option value="2025">2025</option>
											</select>
										</span><!-- .field-wrap -->
									</div>
								</div>
							</div><!-- .grid -->

							<label>CVC</label>
							<span class="field-wrap">
								<input type="tel" name="cc_cvc" placeholder="123">
							</span>
							
							<button class="button white">Submit</button>
							
						</div><!-- .fieldset -->
						
					</form><!-- .body-form -->
				</div>

			</div><!-- .registration-wrapper -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>