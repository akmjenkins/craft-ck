<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-4.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">FAQs</span>
						<span class="subtitle">
							<span>Sed dictuClaritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.m sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">

			<div class="acc with-indicators separated">
			
				<div class="acc-item">
					<div class="acc-item-handle">
						Title for the first FAQ
					</div>
					<div class="acc-item-content">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac luctus sem, vel maximus neque. 
							Mauris nec libero luctus, dignissim erat nec, tempor nulla. Vivamus finibus urna vitae lacus egestas, 
							sed feugiat libero sodales. Cras sollicitudin ultricies mi sit amet fermentum. 
							Nam ut augue tempus est rhoncus sollicitudin. Nunc sed turpis ut metus ultrices fermentum. 
							Nam gravida et lacus at egestas. Proin vel mi sit amet lacus tristique bibendum. Etiam at tortor quis libero vulputate placerat. 
							Quisque viverra, sem sed dignissim viverra, diam nulla aliquam mauris, sed rhoncus mi metus eget nunc. 
							Donec vestibulum ultrices sem.
						</p>
					</div><!-- .acc-item-content -->
				</div><!-- .acc-item -->
				
				<div class="acc-item">
					<div class="acc-item-handle">
						Title for the second FAQ
					</div>
					<div class="acc-item-content">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac luctus sem, vel maximus neque. 
							Mauris nec libero luctus, dignissim erat nec, tempor nulla. Vivamus finibus urna vitae lacus egestas, 
							sed feugiat libero sodales. Cras sollicitudin ultricies mi sit amet fermentum. 
							Nam ut augue tempus est rhoncus sollicitudin. Nunc sed turpis ut metus ultrices fermentum. 
							Nam gravida et lacus at egestas. Proin vel mi sit amet lacus tristique bibendum. Etiam at tortor quis libero vulputate placerat. 
							Quisque viverra, sem sed dignissim viverra, diam nulla aliquam mauris, sed rhoncus mi metus eget nunc. 
							Donec vestibulum ultrices sem.
						</p>
					</div><!-- .acc-item-content -->
				</div><!-- .acc-item -->
				
				<div class="acc-item">
					<div class="acc-item-handle">
						Title for the third FAQ
					</div>
					<div class="acc-item-content">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac luctus sem, vel maximus neque. 
							Mauris nec libero luctus, dignissim erat nec, tempor nulla. Vivamus finibus urna vitae lacus egestas, 
							sed feugiat libero sodales. Cras sollicitudin ultricies mi sit amet fermentum. 
							Nam ut augue tempus est rhoncus sollicitudin. Nunc sed turpis ut metus ultrices fermentum. 
							Nam gravida et lacus at egestas. Proin vel mi sit amet lacus tristique bibendum. Etiam at tortor quis libero vulputate placerat. 
							Quisque viverra, sem sed dignissim viverra, diam nulla aliquam mauris, sed rhoncus mi metus eget nunc. 
							Donec vestibulum ultrices sem.
						</p>
					</div><!-- .acc-item-content -->
				</div><!-- .acc-item -->
				
			</div><!-- .acc -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>