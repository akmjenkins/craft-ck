<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-7.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">News Title</span>
						<span class="subtitle">
							<span>Sed dictuClaritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.m sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
						<a href="#" class="button big blue">Register</a>	
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">

			<article>
				<div class="main-body">
				
					<time class="date-block pubdate" pubdate>
						<span class="dow">Wednesday</span>
						<span class="date">14</span>
						<span class="month">October</span>
					</time>
				
					<div class="content">
					
						<div class="article-body">
							
							<h1>News Title</h1>
							
							<p>
								Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Claritas est etiam processus dynamicus, 
								qui sequitur mutationem consuetudium lectorum. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium 
								lectorum. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Claritas est etiam processus 
								dynamicus, qui sequitur mutationem consuetudium lectorum. Claritas est etiam processus dynamicus, qui sequitur mutationem 
								consuetudium lectorum. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Claritas est 
								etiam processus dynamicus, qui sequitur mutationem 
							</p>
							
						</div><!-- .article-body -->
					
					</div><!-- .content -->
					
					<aside class="sidebar">
						
						<div class="search-mod mod">
						
							<form action="" class="single-form">
								<div class="fieldset">
									<input type="text" name="s" placeholder="Search News...">
									<button class="t-fa fa-search"></button>
								</div><!-- .fieldset -->
							</form><!-- .single-form -->
						
						</div><!-- .search-mod -->
						
						<div class="archives-mod mod">
							<div class="hgroup">
								<h4 class="title">Archives</h4>
							</div><!-- .hgroup -->
							
							<div class="acc with-indicators inv">
							
								<div class="acc-item">
									<div class="acc-item-handle">
										2014
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										<ul>
											<li><a href="#">October (2)</a></li>
											<li><a href="#">September (5)</a></li>
											<li><a href="#">August (6)</a></li>
										</ul>
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->
								
								<div class="acc-item">
									<div class="acc-item-handle">
										2013
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										<ul>
											<li><a href="#">October (2)</a></li>
											<li><a href="#">September (5)</a></li>
											<li><a href="#">August (6)</a></li>
										</ul>
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->
								
							</div><!-- .acc -->
							
						</div><!-- .archives-mod -->

						
					</aside><!-- .sidebar -->
					
				</div><!-- .main-body -->
			</article>
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>