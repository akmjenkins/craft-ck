<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-1.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">Vanessa Smith</span>
						<span class="subtitle">
							<span>Sed dictum sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
						<a href="#" class="button big blue">Register</a>	
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">

			<article>
		
				<div class="article-body flex-article">
				
					<div>
						<h2>Vanessa Smith</h2>
						
						
						<p>
							Sed dictum sem ac hendrerit elementum. Maecenas aliquet ante id tortor bibendum egestas. In eu consectetur augue, ut rutrum
							dolor. Fusce non sagittis ipsum. Integer vel vehicula sapien, sed dapibus eros. Ut id massa lacinia, vestibulum urna quis, condimentum
							mi. Aliquam erat volutpat. Quisque convallis nunc sit amet eros dignissim, auctor aliquam nisl congue. Nullam id ornare nisi.
						</p>
	 
						<p>
							Sed dictum sem ac hendrerit elementum. Maecenas aliquet ante id tortor bibendum egestas. In eu consectetur augue, ut rutrum
							dolor. Fusce non sagittis ipsum. Integer vel vehicula sapien, sed dapibus eros. Ut id massa lacinia, vestibulum urna quis, condimentum
							mi. Aliquam erat volutpat. Quisque convallis nunc sit amet eros dignissim, auctor aliquam nisl congue. Nullam id ornare nisi.
						</p>
					</div>
					
					
					<div class="article-thumb">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/vanessa.jpg"></div>	
						</div><!-- .instructor-thumb -->
					</div><!-- .article-thumb -->
					
				</div><!-- .article-body -->
				
				<div class="article-body flex-article">
				
					<div>
						<h2>Workshops</h2>
						
						<p>
							Sed dictum sem ac hendrerit elementum. Maecenas aliquet ante id tortor bibendum egestas. In eu consectetur augue, ut rutrum
							dolor. Fusce non sagittis ipsum. Integer vel vehicula sapien, sed dapibus eros. Ut id massa lacinia, vestibulum urna quis, condimentum
							mi. Aliquam erat volutpat. Quisque convallis nunc sit amet eros dignissim, auctor aliquam nisl congue. Nullam id ornare nisi.
						</p>
	 
						
						<div class="ib">
							<div class="date-block">
								<span class="dow">Wednesday</span>
								<span class="date">14</span>
								<span class="month">October</span>
							</div>
							
							<a href="#" class="button blue sm block">Register</a>
						</div><!-- .ib -->
						
						<div class="ib">
							<div class="date-block">
								<span class="dow">Thursday</span>
								<span class="date">15</span>
								<span class="month">October</span>
							</div>
							
							<a href="#" class="button blue sm block">Register</a>
						</div><!-- .ib -->
						
					</div>
					
					
					<div class="article-thumb">
						<div class="workshop-thumb">
							<div class="lazybg" data-src="../assets/images/temp/workshops/basketry.jpg"></div>
						</div><!-- .instructor-thumb -->
						<span class="workshop-name">Basketry</span>
					</div><!-- .article-thumb -->
					
				</div><!-- .article-body -->

				
			</article>
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>