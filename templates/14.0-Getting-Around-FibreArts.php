<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-1.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">Getting Around</span>
						<span class="subtitle">
							<span>Sub title, this will disappear responsively, so we'll move the important text in the PSD to the body as an exceprt (ov-article)</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>

	<section>
		<div class="sw">

			<article>
				<div class="article-body ov-article">
					<p>
						For those who haven’t been here before, you should know that it’s a big island! 
						It’s an eight-hour drive from St. John’s to Rocky Harbour. 
						We will be in Gros Morne in the off-season, so the transportation amenities 
						that you will find in urban areas just won’t be available.
					</p>
				</div><!-- .ov-article -->
			</article>
		
			<div class="section-header">
				<h2 class="title">We Recommend</h2>
			</div><!-- .section-header -->
			
			<div class="article-body ov-article">
				<p>
					<strong>Be Independent:</strong> drive your own car, rent a car, or arrange carpooling. <br />
					<strong>Take the Fibre Conference Bus:</strong> Bus: Bus departs from St. John’s and can pick up individuals along the route to Gros Morne. <br />
					The bus will be a fabulous part of the Fibre Conference experience.
				</p>
				
				<p>
					<em>Following is a summary of transportation options for those who cannot be independent.  Read on for information about:</em>
				</p>
				
				<p>
					The Highway Bus – St. John’s to Gros Morne, and back again <br />
					The Conference Bus Pass – getting around in Gros Morne during the conference <br />
					The Airport Bus – it’s a 90 minute drive from the nearest airport – here’s how to get back and forth <br />
					Commercial transport arrangements – air, road, ferry, local taxis, car rentals, and more! <br />
					The Highway Bus – St. John’s to Gros Morne, and back again
				</p>
				
			</div><!-- .ov-article -->
			
			<div class="grid eqh collapse-900">
			
				<div class="col col-3">				
					<div class="item block-item with-button">
						<div class="block-head dark-bg">Highway Bus to Gros Morne</div>
						
						<span class="block-title">Travels from St. John’s to Gros Morne</span>
						
						<span class="block-price">
							$85.00
							<small>+HST</small>
						</span><!-- .block-price -->
						
						<span class="block-subtitle">
							Departs St. Johns: Wednesday 14 of October, Devon House Craft Centre, 59 Duckworth Street & arrives at designated conference bus stops in Gros Morne.
						</span>
						
						<p>
							Live outside of St. John’s and want to catch the bus enroute? The bus may stop at designated gas stations along the Trans Canada Highway. 
							If you’re not in St. John’s, please talk to us to indicate your preferred stop. We will confirm stops and time with you by June 30, 2015. 
							(If you’re arriving in Port aux Basques and have interest in a bus to Gros Morne, please talk to us. We will consider arranging this if there is enough interest).
						</p>
						
						<div class="button-wrap">
							<a href="#" class="button blue">More Info</a>
						</div><!-- .button-wrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3">
					<div class="item block-item with-button">
						<div class="block-head dark-bg">Highway Bus from Gros Morne</div>
						
						<span class="block-title">Travels from Gros Morne to St. John’s</span>
						
						<span class="block-price">
							$85.00
							<small>+HST</small>
						</span><!-- .block-price -->
						
						<span class="block-subtitle">
							Departs St. Johns: Wednesday 14 of October, Devon House Craft Centre, 59 Duckworth Street & arrives at designated conference bus stops in Gros Morne.
						</span>
						
						<p>
							Live outside of St. John’s and want to catch the bus enroute? The bus may stop at designated gas stations along the Trans Canada Highway. 
							If you’re not in St. John’s, please talk to us to indicate your preferred stop. We will confirm stops and time with you by June 30, 2015. 
							(If you’re arriving in Port aux Basques and have interest in a bus to Gros Morne, please talk to us. We will consider arranging this if there is enough interest).
						</p>
						
						<div class="button-wrap">
							<a href="#" class="button blue">More Info</a>
						</div><!-- .button-wrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3">
					<div class="item block-item with-button">
						<div class="block-head dark-bg">Conference Bus Pass</div>
						
						<span class="block-title">Travels throughout conference amongst...</span>
						
						<span class="block-price">
							$60.00
							<small>+HST</small>
						</span><!-- .block-price -->
						
						<span class="block-subtitle">
							The Conference Bus Pass will cover local ground transportation between Designated Conference Bus Stops and workshops and between Designated Conference Bus Stops evening meals and entertainment, the Market Place and on Sunday, the Discovery Centre.
						</span>
						
						<p>
							There are seven Designated Conference Bus Stop’s in Rocky Harbour and Woody Point – if you are using the Conference Bus to get between sites during the conference, 
							you must stay in accommodations near a bus stop (we cannot visit every B&B in the area for pick-up – Bus users must gather at a Designated Conference Bus Stop for pick-up).
						</p>
						
						<div class="button-wrap">
							<a href="#" class="button blue">More Info</a>
						</div><!-- .button-wrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				
			</div><!-- .grid -->

		</div><!-- .sw -->
	</section>
		
	<section>
		<div class="sw">
		
			<div class="section-header">
				<h2 class="title">Tentative Bus Schedule</h2>
			</div><!-- .section-header -->
	
			<div class="tab-wrapper schedule-tabs show-all daily-schedule">
				<div class="tab-controls with-indicators">
				
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>Thursday, October 15</option>
							<option>Friday, October 16</option>
							<option>Saturday, October 17</option>
							<option>Sunday, October 18</option>
						</select>
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
					
					<a class="date-block ib tab-control selected">
						<span class="dow">Thursday</span>
						<span class="date">15</span>
						<span class="month">October</span>
					</a>
					
					<a class="date-block ib tab-control">
						<span class="dow">Friday</span>
						<span class="date">16</span>
						<span class="month">October</span>
					</a>
					
					<a class="date-block ib tab-control">
						<span class="dow">Saturday</span>
						<span class="date">17</span>
						<span class="month">October</span>
					</a>
					
					<a class="date-block ib tab-control">
						<span class="dow">Sunday</span>
						<span class="date">18</span>
						<span class="month">October</span>
					</a>
					
				</div><!-- .tab-controls -->
				<div class="tab-holder">
					
					<div class="tab selected">
					
						<div class="bordered pad-20">
							<h4>Thursday, October 15</h4>
						
							<ul>
								<li><strong>8:00 am</strong> Pick up at Designated Conference Bus Stops and drop off at workshop sites.</li>
								<li><strong>12:00 noon – 1:00 pm</strong> Pick up from workshop sites if needed for Lunch transportation and return.</li>
								<li><strong>4:00 pm</strong> Pick up at workshop sites and drop off at Designated Conference Bus Stops</li>
								<li><strong>6:00 pm</strong> Pick up at Designated Conference Bus Stops and drop off at Fisherman’s Landing Inn/Ocean View Hotel for evening meal and entertainment.</li>
								<li><strong>10:00 pm</strong> Pick up at Fisherman’s Landing Inn / Ocean View Hotel and drop off at Designated Conference Bus Stops.</li>
							</ul>
						</div><!-- .bordered pad-20 -->
						
					</div><!-- .tab -->
					
					<div class="tab">
					
						<div class="bordered pad-20">
							<h4>Friday, October 16</h4>
						
							<ul>
								<li><strong>8:00 am</strong> Pick up at Designated Conference Bus Stops and drop off at workshop sites.</li>
								<li><strong>12:00 noon – 1:00 pm</strong> Pick up from workshop sites if needed for Lunch transportation and return.</li>
								<li><strong>4:00 pm</strong> Pick up at workshop sites and drop off at Designated Conference Bus Stops</li>
								<li><strong>6:00 pm</strong> Pick up at Designated Conference Bus Stops and drop off at Fisherman’s Landing Inn/Ocean View Hotel for evening meal and entertainment.</li>
								<li><strong>10:00 pm</strong> Pick up at Fisherman’s Landing Inn / Ocean View Hotel and drop off at Designated Conference Bus Stops.</li>
							</ul>
						</div><!-- .bordered pad-20 -->
						
					</div><!-- .tab -->

					<div class="tab">
					
						<div class="bordered pad-20">
							<h4>Saturday, October 16</h4>
						
							<ul>
								<li><strong>8:00 am</strong> Pick up at Designated Conference Bus Stops and drop off at workshop sites.</li>
								<li><strong>12:00 noon – 1:00 pm</strong> Pick up from workshop sites if needed for Lunch transportation and return.</li>
								<li><strong>4:00 pm</strong> Pick up at workshop sites and drop off at Designated Conference Bus Stops</li>
								<li><strong>6:00 pm</strong> Pick up at Designated Conference Bus Stops and drop off at Fisherman’s Landing Inn/Ocean View Hotel for evening meal and entertainment.</li>
								<li><strong>10:00 pm</strong> Pick up at Fisherman’s Landing Inn / Ocean View Hotel and drop off at Designated Conference Bus Stops.</li>
							</ul>
						</div><!-- .bordered pad-20 -->
						
					</div><!-- .tab -->

					<div class="tab">
					
						<div class="bordered pad-20">
							<h4>Sunday, October 16</h4>
						
							<ul>
								<li><strong>8:00 am</strong> Pick up at Designated Conference Bus Stops and drop off at workshop sites.</li>
								<li><strong>12:00 noon – 1:00 pm</strong> Pick up from workshop sites if needed for Lunch transportation and return.</li>
								<li><strong>4:00 pm</strong> Pick up at workshop sites and drop off at Designated Conference Bus Stops</li>
								<li><strong>6:00 pm</strong> Pick up at Designated Conference Bus Stops and drop off at Fisherman’s Landing Inn/Ocean View Hotel for evening meal and entertainment.</li>
								<li><strong>10:00 pm</strong> Pick up at Fisherman’s Landing Inn / Ocean View Hotel and drop off at Designated Conference Bus Stops.</li>
							</ul>
						</div><!-- .bordered pad-20 -->
						
					</div><!-- .tab -->

					
				</div><!-- .tab-holder -->
			</div><!-- .tab-wrapper -->
			
			<p class="note dark-bg">
				Participants who choose not to avail of the Conference Bus Pass and would like to source their own transportation (car rental, private vehicle) are 100% responsible for organizing and funding alternatives.
			</p><!-- .note -->

		</div><!-- .sw -->
	</section>
				
	<section>
		<div class="sw">
				
				<div class="section-header">
					<h2 class="title">Arrival and Departure</h2>
				</div><!-- .section-header -->
				
				<div class="grid eqh collapse-750">
				
					<div class="col col-2">
					
						<div class="item block-item">
							<div class="block-head dark-bg">Thursday, October 15, 2015</div>
							
							<span class="block-title">Airport Bus pass from Deer Lake Airport – Gros Morne designated conference bus stops</span>
							
							<span class="block-price">
								$50.00
								<small>+HST</small>
							</span><!-- .block-price -->
							
							<p>
								2:00 pm approx. Pick up from Deer Lake Airport and drop off at accommodation sites.  (The
								pick up time may change according to flight arrival times. We will do our best to reduce
								wait times for everyone, while ensuring all those with Bus Passes are picked up at the
								airport. Also, be aware that travel and unloading time to accommodation sites will be 60 –
								90 minutes.) Additional buses may be scheduled according to demand. Please email
								events@craftcouncil.nl.ca to request additional bus times. You will be notified by June 30 if
								additional buses are scheduled.
							</p>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-2">
					
						<div class="item block-item">
							<div class="block-head dark-bg">Monday, October 19, 2015</div>
							
							<span class="block-title">Rocky Harbour designated stops – Deer Lake Airport</span>
							
							<span class="block-price">
								$50.00
								<small>+HST</small>
							</span><!-- .block-price -->
							
							<p>
								Time 10:00 Pick up from Designated Conference Bus Stops and drop off to Deer Lake Airport.
								This pick up time has been determined based on flight departure times. Please book your
								departure flight to leave no earlier than 2:00 pm.
							</p>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
		
			</div><!-- .sw -->
		</section>
	
	<section>
		<div class="sw">
		
			<div class="tab-wrapper directions-tabs">
				
				<div class="tab-controls with-indicators">
				
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>By Air</option>
							<option>By Water</option>
							<option>By Land</option>
						</select><!-- .tab-controller -->
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
				
					<div class="tab-control selected">
						<span class="t-fa fa-plane">By Air</span>
					</div><!-- .tab-control -->
					
					<div class="tab-control">
						<span class="t-fa fa-tint">By Water</span>
					</div><!-- .tab-control -->
					
					<div class="tab-control">
						<span class="t-fa fa-car">By Land</span>
					</div><!-- .tab-control -->
					
				</div><!-- .tab-controls -->
				
				<div class="tab-holder">
				
					<div class="tab selected">
						<div class="bordered pad-20 article-body">
							Block 1
							<ul>
								<li>Deer Lake Airport is approximately 70 km. from the Conference location in Bonne Bay, which is to the north and about a 60 minute drive via the Trans-Canada Highway (TCH) Route-430N. See deerlakeairport.com for airlines and flights in and out of Deer Lake. See below for information on ground transportation from Deer Lake Airport.</li>
								<li>John’s International Airport is on the East Coast of Newfoundland and an eight hour drive to the Conference location. You travel west on the TCH Route 1West to Deer Lake and then north on Route 430 to Bonne Bay. See the airport website www.stjohnsairport.com for airlines and flights in and out of St. John’s.</li>
								<li>There are some advantages to arriving in St. John’s despite the distance. It’s an opportunity to extend your trip, see some of the east coast, and visit the oldest city in North	America. And there are more and often cheaper flights in and out of St. John’s. See below for information on ground transportation from St. John’s.</li>
								<li>Conference participants are responsible for purchasing their own airline tickets and travel insurance...</li>
							</ul>
							
						</div><!-- .pad-20 -->
					</div><!-- .tab -->
					
					<div class="tab">
						<div class="bordered pad-20 article-body">
							Block 2
							<ul>
								<li>Deer Lake Airport is approximately 70 km. from the Conference location in Bonne Bay, which is to the north and about a 60 minute drive via the Trans-Canada Highway (TCH) Route-430N. See deerlakeairport.com for airlines and flights in and out of Deer Lake. See below for information on ground transportation from Deer Lake Airport.</li>
								<li>John’s International Airport is on the East Coast of Newfoundland and an eight hour drive to the Conference location. You travel west on the TCH Route 1West to Deer Lake and then north on Route 430 to Bonne Bay. See the airport website www.stjohnsairport.com for airlines and flights in and out of St. John’s.</li>
								<li>There are some advantages to arriving in St. John’s despite the distance. It’s an opportunity to extend your trip, see some of the east coast, and visit the oldest city in North	America. And there are more and often cheaper flights in and out of St. John’s. See below for information on ground transportation from St. John’s.</li>
								<li>Conference participants are responsible for purchasing their own airline tickets and travel insurance...</li>
							</ul>
							
						</div><!-- .pad-20 -->
					</div><!-- .tab -->
					
					<div class="tab">
						<div class="bordered pad-20 article-body">
							Block 3
							<ul>
								<li>Deer Lake Airport is approximately 70 km. from the Conference location in Bonne Bay, which is to the north and about a 60 minute drive via the Trans-Canada Highway (TCH) Route-430N. See deerlakeairport.com for airlines and flights in and out of Deer Lake. See below for information on ground transportation from Deer Lake Airport.</li>
								<li>John’s International Airport is on the East Coast of Newfoundland and an eight hour drive to the Conference location. You travel west on the TCH Route 1West to Deer Lake and then north on Route 430 to Bonne Bay. See the airport website www.stjohnsairport.com for airlines and flights in and out of St. John’s.</li>
								<li>There are some advantages to arriving in St. John’s despite the distance. It’s an opportunity to extend your trip, see some of the east coast, and visit the oldest city in North	America. And there are more and often cheaper flights in and out of St. John’s. See below for information on ground transportation from St. John’s.</li>
								<li>Conference participants are responsible for purchasing their own airline tickets and travel insurance...</li>
							</ul>
							
						</div><!-- .pad-20 -->
					</div><!-- .tab -->
					
				</div><!-- .tab-holder -->
			</div><!-- .tab-wrapper -->
			
			<p class="note dark-bg center">
				For specific information about flights check out the airline websites, book on line, contact your travel agent, or the official 
				Fibre Arts 2015 travel agent Jody Power at Carlson Wagonlit Harvey’s Travel <br />
				
				Email <a href="mailto:jpower@harveystravel-cwt.com" class="inline">jpower@harveystravel-cwt.com</a> | Call Toll Free: 1 877 726 1881 | Visit: <a href="http://www.cwtharveystravel.com" class="inline" rel="external">www.cwtharveystravel.com</a>
			</p>
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>