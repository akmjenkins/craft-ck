<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-1.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">Register</span>
						<span class="subtitle">
							<span>Sed dictum sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">

			<div class="registration-wrapper">
			
				<div class="section-header">
					<h2 class="title">Attendee</h2>
				</div><!-- .section-header -->
				
				<div class="body-form reg-contact-info full">
					<input type="text" name="fname" placeholder="First Name">
					<input type="text" name="lname" placeholder="Last Name">
					<input type="email" name="email" placeholder="E-mail">
					<textarea name="notes" cols="30" rows="10" placeholder="Notes..."></textarea>
				</div><!-- .body-form -->
				
				<div class="section-header">
					<h2 class="title">Conference</h2>
				</div><!-- .section-header -->
				
				<div class="reg-section">
					
					<div class="reg-item">
					
						<span class="reg-label">
							Access to registration information, lunches, market places and artist talks.
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$200
						</span><!-- .reg-price -->
						
					</div><!-- .reg-item -->
					
				</div><!-- .reg-section -->
				
				<div class="section-header">
					<h2 class="title">Meals</h2>
				</div><!-- .section-header -->
					
				<div class="reg-section">
					<label class="reg-item t-fa selected">
					
						<input type="checkbox" name="meals[wednesday]">
						
						<span class="reg-label">
							Wednesday
							<span class="reg-label-meta">Location A</span>
							<span class="reg-label-meta">Entertainment A</span>
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
					
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="meals[thursday]">
						
						<span class="reg-label">
							Thursday
							<span class="reg-label-meta">Location A</span>
							<span class="reg-label-meta">Entertainment A</span>
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
					
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="meals[friday]">
						
						<span class="reg-label">
							Friday
							<span class="reg-label-meta">Location A or Location B</span>
							<span class="reg-label-meta">Entertainment A or Entertainment B</span>
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
					
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="meals[saturday]">
						
						<span class="reg-label">
							Saturday
							<span class="reg-label-meta">Location A</span>
							<span class="reg-label-meta">Entertainment A</span>
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
					
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="meals[free]">
						
						<span class="reg-label">
							Free Dinner
							<span class="reg-label-meta">Please tick this box to RSVP</span>
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							Free
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
				</div><!-- .reg-section -->
				
				<div class="section-header">
					<h2 class="title">Activities</h2>
				</div><!-- .section-header -->
					
				<div class="reg-section">
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="activities[walking-tour]">
						
						<span class="reg-label">
							Walking Tour
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
					
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="activities[artist-tour]">
						
						<span class="reg-label">
							Artist Tour
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
				</div><!-- .reg-section -->

				<div class="section-header">
					<h2 class="title">Transportation</h2>
				</div><!-- .section-header -->
					
				<div class="reg-section">
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="transportation[slug-1]">
						
						<span class="reg-label">
							St. John's Airport to Conference
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
					
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="transportation[slug-2]">
						
						<span class="reg-label">
							Deer Lake Airport to Conference
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
					
					<label class="reg-item t-fa">
					
						<input type="checkbox" name="transportation[slug-3]">
						
						<span class="reg-label">
							Conference Bus Pass
						</span><!-- .reg-label -->
						
						<span class="reg-item-price">
							$50
						</span><!-- .reg-price -->
						
					</label><!-- .reg-item -->
				</div><!-- .reg-section -->

				<div class="active-registrations">
				
					<div class="active-registration-item">
					
						<span class="t-fa fa-close delete-item">&nbsp;</span>
					
						<span class="active-registration-name">John Atkins</span>
						<span class="active-registration-total">$525.00</span>
					
					</div><!-- .active-registration-item -->
					
				</div><!-- .active-registrations -->
				
				<div class="reg-buttons">
					<button class="button green">Save &amp; Add Person</button>
					<button class="button grey">Reset Form</button>
					<button class="button blue">Proceed to Payment</button>
				</div><!-- .reg-buttons -->
				
			</div><!-- .registration-wrapper -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>