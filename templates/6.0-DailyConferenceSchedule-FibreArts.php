<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-8.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">Daily Conference Schedule</span>
						<span class="subtitle">
							<span>Sed dictuClaritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.m sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>

	<section>
		<div class="sw">

			<div class="section-header">
				<h2 class="title">Daily Conference Schedule</h2>
			</div><!-- .section-header -->
			
			<!-- 
				There's an alternate way to display the schedule using tabs below the schedule-grid
				The alternate way is required on mobile, and it's much more flexible (allows for much more variable text) than the grid
				way of displaying events, even on desktop
			-->
		
			<div class="schedule-grid">
				
				<table>
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th class="today">
								<span class="date-block">
									<span class="dow">Wednesday</span>
									<span class="date">14</span>
									<span class="month">October</span>
								</span><!-- .date-block -->
							</th>
							<th>
								<span class="date-block">
									<span class="dow">Thursday</span>
									<span class="date">15</span>
									<span class="month">October</span>
								</span><!-- .date-block -->
							</th>
							<th>
								<span class="date-block">
									<span class="dow">Friday</span>
									<span class="date">16</span>
									<span class="month">October</span>
								</span><!-- .date-block -->
							</th>
							<th>
								<span class="date-block">
									<span class="dow">Saturday</span>
									<span class="date">17</span>
									<span class="month">October</span>
								</span><!-- .date-block -->
							</th>
							<th>
								<span class="date-block">
									<span class="dow">Sunday</span>
									<span class="date">18</span>
									<span class="month">October</span>
								</span><!-- .date-block -->
							</th>
						</tr>
					</thead>
					<tbody>
						<!-- add a class of inactive to all rows that are already in the past -->
						<tr class="inactive">
							<td>
							
								<span class="schedule-item-time">9 AM</span>
								
							</td>
							<td class="today">
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
						</tr>

						<tr class="inactive">
							<td>
							
								<span class="schedule-item-time">10 AM</span>
								
							</td>
							<td class="today">
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
						</tr>

						<tr class="inactive">
							<td>
							
								<span class="schedule-item-time">11 AM</span>
								
							</td>
							<td class="today">
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
						</tr>

						<tr class="inactive">
							<td>
							
								<span class="schedule-item-time">12 PM</span>
								
							</td>
							<td class="today">
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
						</tr>

						<!-- add a class of current to the row that are ongoing -->
						<tr class="current">
							<td>
							
								<span class="schedule-item-time">1 PM</span>
								
							</td>
							<td class="today">
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
						</tr>

						<tr>
							<td>
							
								<span class="schedule-item-time">2 PM</span>
								
							</td>
							<td class="today">
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
						</tr>

						<tr>
							<td>
							
								<span class="schedule-item-time">3 PM</span>
								
							</td>
							<td class="today">
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
						</tr>

						<tr>
							<td>
							
								<span class="schedule-item-time">4 PM</span>
								
							</td>
							<td class="today">
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
							<td>
								<span class="schedule-item-title">Title</span>
								<span class="schedule-item-location">Location</span>
							</td>
						</tr>

					</tbody>
				</table>
				
			</div><!-- .schedule-grid -->
		
			<div class="tab-wrapper schedule-tabs daily-schedule">
				<div class="tab-controls">
				
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>Wednsday, October 14</option>
							<option>Thursday, October 15</option>
							<option>Friday, October 16</option>
							<option>Saturday, October 17</option>
							<option>Sunday, October 18</option>
						</select>
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
					
					<a class="date-block ib tab-control selected">
						<span class="dow">Wednesday</span>
						<span class="date">14</span>
						<span class="month">October</span>
					</a>
					
					<a class="date-block ib tab-control">
						<span class="dow">Thursday</span>
						<span class="date">15</span>
						<span class="month">October</span>
					</a>
					
					<a class="date-block ib tab-control">
						<span class="dow">Friday</span>
						<span class="date">16</span>
						<span class="month">October</span>
					</a>
					
					<a class="date-block ib tab-control">
						<span class="dow">Saturday</span>
						<span class="date">17</span>
						<span class="month">October</span>
					</a>
					
					<a class="date-block ib tab-control">
						<span class="dow">Sunday</span>
						<span class="date">18</span>
						<span class="month">October</span>
					</a>
					
				</div><!-- .tab-controls -->
				<div class="tab-holder">
					
					<div class="tab selected">
					
						<div class="schedule-block">
					
							<!-- this can also be an A tag is necessary -->
							<!-- add a class of inactive if this item is older than the current time -->
							<div class="schedule-item inactive">							
								<span class="schedule-item-time">9 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
							
							<div class="schedule-item inactive">
								<span class="schedule-item-time">10 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item inactive">
								<span class="schedule-item-time">11 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item inactive">
								<span class="schedule-item-time">12 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item inactive">
								<span class="schedule-item-time">1 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<!-- add a class of current if this item is ongoing -->
							<div class="schedule-item current">
								<span class="schedule-item-time">2 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">3 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">4 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->								
							</div><!-- .schedule-item -->
					
						</div><!-- .schedule-block -->
					
					</div><!-- .tab -->
					
					<div class="tab">
					
						<div class="schedule-block">
					
							<!-- this can also be an A tag is necessary -->
							<!-- add a class of inactive if this item is older than the current time -->
							<div class="schedule-item">							
								<span class="schedule-item-time">9 am - 1</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
								
								<div class="workshop-item">
									<div class="workshop-thumb">
										<div class="lazybg" data-src="../assets/images/temp/workshops/basketry.jpg"></div>
									</div><!-- .workshop-thumb -->
									<span class="workshop-name">Basketry</span>
								</div><!-- .workshop-item -->
								
							</div><!-- .schedule-item -->
							
							<div class="schedule-item">
								<span class="schedule-item-time">10 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
								
								<div class="workshop-item">
									<div class="workshop-thumb">
										<div class="lazybg" data-src="../assets/images/temp/workshops/dye.jpg"></div>
									</div><!-- .workshop-thumb -->
									<span class="workshop-name">Dye</span>
								</div><!-- .workshop-item -->
								
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">11 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
								
								<div class="workshop-item">
									<div class="workshop-thumb">
										<div class="lazybg" data-src="../assets/images/temp/workshops/felting.jpg"></div>
									</div><!-- .workshop-thumb -->
									<span class="workshop-name">Felting</span>
								</div><!-- .workshop-item -->
								
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">12 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
								
								<div class="workshop-item">
									<div class="workshop-thumb">
										<div class="lazybg" data-src="../assets/images/temp/workshops/knitting.jpg"></div>
									</div><!-- .workshop-thumb -->
									<span class="workshop-name">Knitting</span>
								</div><!-- .workshop-item -->
								
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">1 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
								
								<div class="workshop-item">
									<div class="workshop-thumb">
										<div class="lazybg" data-src="../assets/images/temp/workshops/knitting.jpg"></div>
									</div><!-- .workshop-thumb -->
									<span class="workshop-name">Knitting</span>
								</div><!-- .workshop-item -->
							</div><!-- .schedule-item -->
								
							<!-- add a class of current if this item is ongoing -->
							<div class="schedule-item current">
								<span class="schedule-item-time">2 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
								
								<div class="workshop-item">
									<div class="workshop-thumb">
										<div class="lazybg" data-src="../assets/images/temp/workshops/knitting.jpg"></div>
									</div><!-- .workshop-thumb -->
									<span class="workshop-name">Knitting</span>
								</div><!-- .workshop-item -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">3 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
								
								<div class="workshop-item">
									<div class="workshop-thumb">
										<div class="lazybg" data-src="../assets/images/temp/workshops/knitting.jpg"></div>
									</div><!-- .workshop-thumb -->
									<span class="workshop-name">Knitting</span>
								</div><!-- .workshop-item -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">4 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->	

								<div class="workshop-item">
									<div class="workshop-thumb">
										<div class="lazybg" data-src="../assets/images/temp/workshops/knitting.jpg"></div>
									</div><!-- .workshop-thumb -->
									<span class="workshop-name">Knitting</span>
								</div><!-- .workshop-item -->
							</div><!-- .schedule-item -->
					
						</div><!-- .schedule-block -->
						
					</div><!-- .tab -->

					<div class="tab">
					
						<div class="schedule-block">
					
							<!-- this can also be an A tag is necessary -->
							<!-- add a class of inactive if this item is older than the current time -->
							<div class="schedule-item">							
								<span class="schedule-item-time">9 am - 2</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
							
							<div class="schedule-item">
								<span class="schedule-item-time">10 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">11 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">12 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">1 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<!-- add a class of current if this item is ongoing -->
							<div class="schedule-item current">
								<span class="schedule-item-time">2 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">3 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">4 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->								
							</div><!-- .schedule-item -->
					
						</div><!-- .schedule-block -->
						
					</div><!-- .tab -->
					
					<div class="tab">
					
						<div class="schedule-block">
					
							<!-- this can also be an A tag is necessary -->
							<!-- add a class of inactive if this item is older than the current time -->
							<div class="schedule-item">							
								<span class="schedule-item-time">9 am - 3</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
							
							<div class="schedule-item">
								<span class="schedule-item-time">10 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">11 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">12 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">1 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<!-- add a class of current if this item is ongoing -->
							<div class="schedule-item current">
								<span class="schedule-item-time">2 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">3 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">4 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->								
							</div><!-- .schedule-item -->
					
						</div><!-- .schedule-block -->
						
					</div><!-- .tab -->

					<div class="tab">
					
						<div class="schedule-block">
					
							<!-- this can also be an A tag is necessary -->
							<!-- add a class of inactive if this item is older than the current time -->
							<div class="schedule-item">							
								<span class="schedule-item-time">9 am - 4</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
							
							<div class="schedule-item">
								<span class="schedule-item-time">10 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">11 am</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">12 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">1 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<!-- add a class of current if this item is ongoing -->
							<div class="schedule-item current">
								<span class="schedule-item-time">2 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">3 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->
							</div><!-- .schedule-item -->
								
							<div class="schedule-item">
								<span class="schedule-item-time">4 pm</span>
								<span class="schedule-item-content">
									<span class="schedule-item-title">Schedule Item Title</span>
									<span class="schedule-item-location">Schedule Item Location</span>
								</span><!-- .schedule-item-content -->								
							</div><!-- .schedule-item -->
					
						</div><!-- .schedule-block -->
						
					</div><!-- .tab -->					

					
				</div><!-- .tab-holder -->
			</div><!-- .tab-wrapper -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>