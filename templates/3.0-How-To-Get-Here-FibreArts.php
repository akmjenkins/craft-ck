<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-5.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">How To Get Here</span>
						<span class="subtitle">
							<span>Sed dictum sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
						<a href="#" class="button big blue">Register</a>	
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">
		
			<div class="section-header">
				<h2 class="title">If You're Travelling...</h2>
			</div><!-- .section-header -->
			
			<div class="tab-wrapper directions-tabs">
			
				<div class="tab-controls">
				
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>By Air</option>
							<option>By Water</option>
							<option>By Land</option>
						</select><!-- .tab-controller -->
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
				
					<div class="tab-control selected">
						<span class="t-fa fa-plane">By Air</span>
					</div><!-- .tab-control -->
					
					<div class="tab-control">
						<span class="t-fa fa-tint">By Water</span>
					</div><!-- .tab-control -->
					
					<div class="tab-control">
						<span class="t-fa fa-car">By Land</span>
					</div><!-- .tab-control -->
				
				</div><!-- .tab-controls -->
			
				<div class="tab-holder">
					
					<div class="tab selected">
						<div class="embedded-gmap">
							<iframe
								frameborder="0" style="border:0"
								src="https://www.google.com/maps/embed/v1/directions?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>
										  &mode=flying&origin=St. John's International Airport&destination=Rocky Harbour, NL">
							</iframe>
						</div><!-- .embedded-gmap -->
					
					</div><!-- .tab -->
					
					<div class="tab">
					
						<div class="embedded-gmap">
							<iframe
								frameborder="0" style="border:0"
								src="https://www.google.com/maps/embed/v1/directions?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>
										  &origin=Marina+Atlantic, Channel Port Aux Basques
										  &destination=Rocky Harbour, NL">
							</iframe>
						</div><!-- .embedded-gmap -->
					
					</div><!-- .tab -->
					
					<div class="tab">
					
						<strong class="uc block center">From Address:</strong>
					
						<form action="/" class="single-form" id="street-form">
							<div class="fieldset">
								<input type="text" placeholder="Enter your address...">
								<button type="submit" class="t-fa fa-search"></button>
							</div><!-- .fieldset -->
							<button id="directions-geolocate" type="button" class="current-location t-fa fa-location-arrow"></button>
						</form>
						
						<div id="street-map" class="embedded-gmap" data-destination="Rocky Harbour, NL">
						</div><!-- #street-map -->
					
					
					</div><!-- .tab -->
					
				</div><!-- .tab-holder -->
				
			</div><!-- .tab-wrapper -->

		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>