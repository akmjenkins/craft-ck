<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-2.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">Talk to Us</span>
						<span class="subtitle">
							<span>Sed dictum sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">

			<div class="contact-page-wrap">
				<div class="grid pad40 collapse-800">
				
					<div class="col col-2">
						<div class="item">
							
							<strong class="uc block">Craft Council</strong>
							<br />
							
							<div class="address-block">
								
								<address>
									Devon House and Craft Centre <br />
									59 Duckworth Street <br />
									St. John's, NL A1C 1E6
								</address>
								
								<div class="rows">
									<div class="row">
										<span class="l">Local:</span>
										<span class="r">709 722 5741</span>
									</div>
									<div class="row">
										<span class="l">Toll Free:</span>
										<span class="r">800 285 8870</span>
									</div>
									<div class="row">
										<span class="l">After Hours:</span>
										<span class="r">709 834 4190</span>
									</div>
								</div><!-- .rows -->
								
							</div><!-- .address-block -->
							
							<br />
							<br />
							
							<strong class="uc block">Hours of Operation</strong>
							<br />
							
								<div class="rows">
								
									<div class="row">
										<span class="l">Monday - Friday &mdash;</span>
										<span class="r">9:00am - 5:00pm</span>
									</div><!-- .row -->
									
									<div class="row">
										<span class="l">Saturday &mdash;</span>
										<span class="r">9:00am - 5:00pm</span>
									</div><!-- .row -->
									
									<div class="row">
										<span class="l">Sunday &mdash;</span>
										<span class="r">Closed</span>
									</div><!-- .row -->
									
								</div><!-- .rows -->
							
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-2">
						<div class="item">
							
							<form action="" class="body-form">
							
								<input type="text" name="name" placeholder="Name">
								<input type="email" name="email" placeholder="E-mail">
								<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>
							
								<button class="button blue">Submit</button>
								
							</form><!-- .body-form -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
			</div><!-- .contact-page-wrap -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>