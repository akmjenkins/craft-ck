			<footer class="d-bg">
				<div class="sw footer-wrap">
				
					<div class="lazybg footer-logo">
						<img src="../assets/images/fibre-arts-council-white.png" alt="Fibre Arts 2015 Logo White">
					</div><!-- .lazybg -->
				
					<div class="footer-blocks">
					
						<div class="footer-block footer-contact-block">
						
							<h6>Contact Us</h6>
							
							<span class="footer-contact-item">
								Phillipa Jones
								<em>Fibre Conference Coordinator</em>
							</span>
							
							<span class="footer-contact-item">
								Craft Council of Newfoundland and Labrador
							</span>
							
							<span class="footer-contact-item">
								<address>
									Devon House and Craft Centre <br />
									59 Duckworth Street <br />
									St. John's, NL A1C 1E6
								</address>
							</span>
							
							<div class="rows">
								<div class="row">
									<span class="l">Tel:</span>
									<span class="r">(709) 753-2749</span>
								</div>
								<div class="row">
									<span class="l">Fax:</span>
									<span class="r">(709) 753-2766</span>
								</div>
								<div class="row">
									<span class="l">Email:</span>
									<span class="r"><a href="#">events@craftcouncil.nl.ca</a></span>
								</div>
							</div><!-- .rows -->
							
							
						
						</div><!-- .footer-block -->
						
						<div class="footer-block footer-nav-block">
						
							<h6>Main Links</h6>
						
							<ul>
								<li><a href="#">About</a></li>
								<li><a href="#">Plan Your Stay</a></li>
								<li><a href="#">Instructors</a></li>
								<li><a href="#">Workshops</a></li>
								<li><a href="#">Schedule</a></li>
								<li><a href="#">Events</a></li>
								<li><a href="#">Become a Sponsor</a></li>
								<li><a href="#">News</a></li>
								<li><a href="#">Talk to Us</a></li>
							</ul>
							
						</div><!-- .footer-block -->
						
						<div class="footer-block footer-meta-block">
						
							<h6>About Fibre Arts NL Conference</h6>
						
							<p>
								The Craft Council of Newfoundland and Labrador is a member-based
								organization that works to maximize the artistic and economic
								potential of the craft community of the province.
							</p>

							<p>
								The Craft Council of Newfoundland and Labrador is a voluntary,
								charitable organization and a member of the Canadian Crafts
								Federation. A professional staff supports the Board of Directors and
								numerous standing committees in the management of the Council's
								operations.
							</p>
							
							<a href="#" class="button blue big">Register</a>
						
						</div><!-- .footer-block -->
						
					</div><!-- .footer-blocks -->
				
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">							
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Fibre Arts NL</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<div class="social">
							<a href="#" title="Like Fibre Arts on Facebook" class="social-fb" rel="external">Like Fibre Arts on Facebook</a>
							<a href="#" title="Follow Fibre Arts on Twitter" class="social-tw" rel="external">Follow Fibre Arts on Twitter</a>
						</div><!-- .social -->
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
		
		<form action="/" method="get" class="global-search-form">
			<div class="fieldset">
				<input type="search" name="s" placeholder="Search...">
				<span class="close t-fa-abs fa-close toggle-search">Close</span>
			</div>
		</form><!-- .single-form -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/craft'
			};
		</script>
		
		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>