			<div class="section-header">
				<h2 class="title">Our Sponsors</h2>
				<span class="subtitle">We have amazing sponsors.</span>
			</div><!-- .section-header -->			
			
			<div class="sponsors-grid-wrap">
				<div class="grid sponsors-grid centered">
					<div class="col col-4 sm-col-2 xs-col-1">
						<a href="#" rel="external" class="item sponsor-item lazybg">
							<img src="http://dummyimage.com/472x250/ffffff/000" alt="dummy image">
						</a><!-- .sponsor-item -->
					</div><!-- .col -->
					<div class="col col-4 sm-col-2 xs-col-1">
						<a href="#" rel="external" class="item sponsor-item lazybg">
							<img src="http://dummyimage.com/472x250/ffffff/000" alt="dummy image">
						</a><!-- .sponsor-item -->
					</div><!-- .col -->
					<div class="col col-4 sm-col-2 xs-col-1">
						<a href="#" rel="external" class="item sponsor-item lazybg">
							<img src="http://dummyimage.com/472x250/ffffff/000" alt="dummy image">
						</a><!-- .sponsor-item -->
					</div><!-- .col -->
					<div class="col col-4 sm-col-2 xs-col-1">
						<a href="#" rel="external" class="item sponsor-item lazybg">
							<img src="http://dummyimage.com/472x250/ffffff/000" alt="dummy image">
						</a><!-- .sponsor-item -->
					</div><!-- .col -->
				</div><!-- .grid -->
			</div><!-- .sponsors-grid-wrap -->
