			<div class="section-header">
				<h2 class="title">Featured Instructors</h2>
				<span class="subtitle">We have amazing instructors.</span>
			</div><!-- .section-header -->
			
			<div class="grid instructors-grid pad40">
			
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/vanessa.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Vanessa Smith</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/john.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">John Quincy</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/tamara.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Tamara Shockey</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/tim.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Tim Sparks</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/cameron.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Cameron Weyland</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/kim.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Kim Johnson</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/winston.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Winston Buckley</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/steve.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Steve White</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/betsy.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Betsy Fern</span>
					</a>
				</div><!-- .col -->
				
				<div class="col">
					<a class="item instructor-grid-item" href="#">
						<div class="instructor-thumb">
							<div class="lazybg" data-src="../assets/images/temp/instructors/sam.jpg"></div>	
						</div>
						<span class="hex-separator"><span></span></span>
						<span class="instructor-name">Sam Watts</span>
					</a>
				</div><!-- .col -->
				
			</div><!-- .instructors-grid -->
