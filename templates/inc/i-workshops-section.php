			<div class="workshop-grid-wrap">
				<div class="grid workshop-grid">
				
					<div class="col">
						<a class="item workshop-item" href="#">
							<div class="workshop-thumb">
								<div class="lazybg" data-src="../assets/images/temp/workshops/basketry.jpg"></div>
							</div><!-- .workshop-thumb -->
							<span class="hex-separator"><span></span></span>
							<span class="workshop-name">Basketry</span>
						</a>
					</div><!-- .col -->
					<div class="col">
						<a class="item workshop-item" href="#">
							<div class="workshop-thumb">
								<div class="lazybg" data-src="../assets/images/temp/workshops/dye.jpg"></div>
							</div><!-- .workshop-thumb -->
							<span class="hex-separator"><span></span></span>
							<span class="workshop-name">Dye</span>
						</a>
					</div><!-- .col -->
					<div class="col">
						<a class="item workshop-item" href="#">
							<div class="workshop-thumb">
								<div class="lazybg" data-src="../assets/images/temp/workshops/felting.jpg"></div>
							</div><!-- .workshop-thumb -->
							<span class="hex-separator"><span></span></span>
							<span class="workshop-name">Felting</span>
						</a>
					</div><!-- .col -->
					<div class="col">
						<a class="item workshop-item" href="#">
							<div class="workshop-thumb">
								<div class="lazybg" data-src="../assets/images/temp/workshops/knitting.jpg"></div>
							</div><!-- .workshop-thumb -->
							<span class="hex-separator"><span></span></span>
							<span class="workshop-name">Knitting</span>
						</a>
					</div><!-- .col -->
					<div class="col">
						<a class="item workshop-item" href="#">
							<div class="workshop-thumb">
								<div class="lazybg" data-src="../assets/images/temp/workshops/rug-hooking.jpg"></div>
							</div><!-- .workshop-thumb -->
							<span class="hex-separator"><span></span></span>
							<span class="workshop-name">Rug Hooking</span>
						</a>
					</div><!-- .col -->
					<div class="col">
						<a class="item workshop-item" href="#">
							<div class="workshop-thumb">
								<div class="lazybg" data-src="../assets/images/temp/workshops/spinning.jpg"></div>
							</div><!-- .workshop-thumb -->
							<span class="hex-separator"><span></span></span>
							<span class="workshop-name">Spinning</span>
						</a>
					</div><!-- .col -->
					<div class="col">
						<a class="item workshop-item" href="#">
							<div class="workshop-thumb">
								<div class="lazybg" data-src="../assets/images/temp/workshops/stitching.jpg"></div>
							</div><!-- .workshop-thumb -->
							<span class="hex-separator"><span></span></span>
							<span class="workshop-name">Stitching</span>
						</a>
					</div><!-- .col -->
					<div class="col">
						<a class="item workshop-item" href="#">
							<div class="workshop-thumb">
								<div class="lazybg" data-src="../assets/images/temp/workshops/weaving.jpg"></div>
							</div><!-- .workshop-thumb -->
							<span class="hex-separator"><span></span></span>
							<span class="workshop-name">Weaving</span>
						</a>
					</div><!-- .col -->
				</div><!-- .workshops-grid -->
				
				<div class="center">
					<a href="#" class="button blue big">Other</a>
				</div><!-- .center -->
				
			</div><!-- .workshop-grid-wrap -->