<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<button class="t-fa-abs fa-search toggle-search mobile-toggle-search">
	Search
</button>

<div class="nav">
	<div class="sw">
		
		<nav>
			<ul>
				<li class="active">
					<a href="#">About</a>
					
					<div class="dd-wrap">
						<div class="dd">
						
							<div class="dd-meta-left dark-bg">
								
								<div class="dd-left-item">
									
									<span class="dd-left-item-title">About</span>
									<p>
										Claritas est etiam processus dynamicus, qui
										sequitur mutationem consuetudium
										lectorum. Claritas est etiam processus
										dynamicus, qui sequitur mutationem
										consuetudium lectorum.
									</p>
									
									<a href="#" class="button sm">More</a>
									
								</div><!-- .dd-left-item -->
								
								<div class="dd-left-item">
									<?php include('i-nav-craft-council.php'); ?>
								</div><!-- .dd-left-item -->
								
							</div><!-- .dd-left-meta -->
							
							<div class="dd-main">
							
								<div class="dd-menu">
								
									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Fibre Arts NL Conference</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button blue sm">More</span>
									</a><!-- .dd-menu-item -->
									
									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Sponsors</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button blue sm">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">FAQs</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button blue sm">More</span>
									</a><!-- .dd-menu-item -->
									
									<div class="dd-contact">
										<?php include('i-nav-contact.php'); ?>
									</div><!-- .dd-menu-item -->
									
									<div class="dd-menu-meta">
										<div class="lazybg" data-src="../assets/images/temp/dd-photo.jpg"></div>
										
										<div class="dd-menu-meta-content">
											
											<span class="dd-menu-title">Did You Know?</span>
											
											<p>
												Sed dictum sem ac hendrerit elementum.
												Maecenas aliquet ante id tortor bibendum
												egestas. In eu consectetur augue, ut rutrum
												dolor. Fusce non sagittis ipsum. Integer vel
												vehicula sapien, sed dapibus eros. Ut id massa
												lacinia, vestibulum urna quis
											</p>
											
										</div>									
									</div><!-- .dd-menu-item -->
									
								</div><!-- .dd-menu -->
								
							</div><!-- .dd-main -->
						
						</div><!-- .dd -->
					</div><!-- .dd-wrap -->
				</li>
				<li>
					<a href="#">Plan Your Stay</a>
					
					<div class="dd-wrap">
						<div class="dd">
						
							<div class="dd-meta-left dark-bg">
								
								<div class="dd-left-item">
									
									<span class="dd-left-item-title">Plan Your Stay</span>
									<p>
										Claritas est etiam processus dynamicus, qui
										sequitur mutationem consuetudium
										lectorum. Claritas est etiam processus
										dynamicus, qui sequitur mutationem
										consuetudium lectorum.
									</p>
									
									<a href="#" class="button sm">More</a>
									
								</div><!-- .dd-left-item -->
								
								<div class="dd-left-item">
									<?php include('i-nav-craft-council.php'); ?>
								</div><!-- .dd-left-item -->
								
							</div><!-- .dd-meta-left -->
							
							<div class="dd-main">
							
								<div class="dd-menu">

									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">How to Get Here</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->
									
									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Accommodations</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->
									
									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Places to Eat</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->
									
									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Things to Do</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->
								
									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Getting Around</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<div class="dd-contact">
										<?php include('i-nav-contact.php'); ?>
									</div><!-- .dd-menu-item -->
									
									<div class="dd-menu-meta">
										
										<div class="dd-menu-meta-content">
											
											<span class="dd-menu-title">Did You Know?</span>
											
											<p>
												Sed dictum sem ac hendrerit elementum.
												Maecenas aliquet ante id tortor bibendum
												egestas. In eu consectetur augue, ut rutrum
												dolor. Fusce non sagittis ipsum. Integer vel
												vehicula sapien, sed dapibus eros. Ut id massa
												lacinia, vestibulum urna quis
											</p>
											
										</div>									
									</div><!-- .dd-menu-meta -->
									
								</div><!-- .dd-menu -->
								
							</div><!-- .dd-main -->
							
						</div><!-- .dd -->
					</div><!-- .dd-wrap -->
					
				</li>
				<li>
					<a href="#">Instructors</a>
					
					<div class="dd-wrap">
						<div class="dd">

							<div class="dd-meta-left dark-bg">
								
								<div class="dd-left-item">
									
									<span class="dd-left-item-title">Instructors</span>
									<p>
										Claritas est etiam processus dynamicus, qui
										sequitur mutationem consuetudium
										lectorum. Claritas est etiam processus
										dynamicus, qui sequitur mutationem
										consuetudium lectorum.
									</p>
									
									<a href="#" class="button sm">More</a>
									
								</div><!-- .dd-left-item -->
								
								<div class="dd-left-item">
									<?php include('i-nav-craft-council.php'); ?>
								</div><!-- .dd-left-item -->
								
							</div><!-- .dd-meta-left -->
							
							<div class="dd-main">

								<div class="instructors-swiper-wrapper">
									
									<span class="dd-left-item-title title">Instructors (10)</span>
									
									<div class="instructor-swiper-wrapper swiper-wrapper-to-be">
										<div class="swiper" data-dots="true" data-arrows="false">
										
											<div class="swipe-item">
											
												<div class="instructor-grid-wrap">
													<div class="instructors-grid grid">
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/vanessa.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Vanessa Smith</span>
														</a>
													</div>
													
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/john.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">John Quincy</span>
														</a>
													</div>
													
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/tamara.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Tamara Shockey</span>
														</a>
													</div>
													
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/tim.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Tim Sparks</span>
														</a>
													</div>
													
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/cameron.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Cameron Weyland</span>
														</a>
													</div>
													</div><!-- .instructor-grid -->
													
												</div><!-- .instructor-grid-wrap -->
											
											</div><!-- .swipe-item -->
											
											<div class="swipe-item">
												<div class="instructor-grid-wrap">
													<div class="instructors-grid grid">
											
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/kim.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Kim Johnson</span>
														</a>
													</div>
													
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/winston.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Winston Buckley</span>
														</a>
													</div>
													
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/steve.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Steve White</span>
														</a>
													</div>
													
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/betsy.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Betsy Fern</span>
														</a>
													</div>
													
													<div class="swipe-item col">
														<a class="item instructor-grid-item" href="#">
															<div class="instructor-thumb">
																<div class="lazybg" data-src="../assets/images/temp/instructors/sam.jpg"></div>	
															</div>
															<span class="hex-separator"><span></span></span>
															<span class="instructor-name">Sam Watts</span>
														</a>
													</div>

													</div><!-- .instrcutor-grid -->
													
												</div><!-- .instructor-grid-wrap -->
											</div><!-- .swipe-item -->
										
										</div><!-- .swiper -->
									</div><!-- .instructor-swiper-wrapper -->
							
								</div><!-- .instructor-grid-wrap -->
								
							</div><!-- .dd-main -->

						</div><!-- .dd -->
					</div><!-- .dd-wrap -->
				</li>
				<li>
					<a href="#">Workshops</a>
					
					<div class="dd-wrap">
						<div class="dd">
						
							<div class="dd-meta-left dark-bg">
								
								<div class="dd-left-item">
									
									<span class="dd-left-item-title">Workshops</span>
									<p>
										Claritas est etiam processus dynamicus, qui
										sequitur mutationem consuetudium
										lectorum. Claritas est etiam processus
										dynamicus, qui sequitur mutationem
										consuetudium lectorum.
									</p>
									
									<a href="#" class="button sm">More</a>
									
								</div><!-- .dd-left-item -->
								
								<div class="dd-left-item">
									<?php include('i-nav-craft-council.php'); ?>
								</div><!-- .dd-left-item -->
								
							</div><!-- .dd-meta-left -->
							
							<div class="dd-main">								
								<div class="workshop-grid-wrap">
									<div class="grid workshop-grid">
									
										<!-- 
											1. no dd-contact on this dropdown....it takes up critical and much needed space 
										-->
									
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/basketry.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Basketry</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/dye.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Dye</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/felting.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Felting</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/knitting.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Knitting</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/rug-hooking.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Rug Hooking</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/spinning.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Spinning</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/stitching.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Stitching</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/weaving.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Weaving</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/design.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Design</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/craftism.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Craftism</span>
											</a>
										</div><!-- .col -->
										<div class="col">
											<a class="item workshop-item" href="#">
												<div class="workshop-thumb">
													<div class="lazybg" data-src="../assets/images/temp/workshops/paper.jpg"></div>
												</div><!-- .workshop-thumb -->
												<span class="hex-separator"><span></span></span>
												<span class="workshop-name">Paper</span>
											</a>
										</div><!-- .col -->
									</div><!-- .workshops-grid -->
								</div><!-- .workshop-grid-wrap -->
							</div><!-- .dd-main -->
							
						</div><!-- .dd -->
					</div><!-- .dd-wrap -->
				</li>
				<li>
					<a href="#">Schedule</a>
					
					<div class="dd-wrap">
						<div class="dd">
						
							<div class="dd-meta-left dark-bg">
								
								<div class="dd-left-item">
									
									<span class="dd-left-item-title">Schedule</span>
									<p>
										Claritas est etiam processus dynamicus, qui
										sequitur mutationem consuetudium
										lectorum. Claritas est etiam processus
										dynamicus, qui sequitur mutationem
										consuetudium lectorum.
									</p>
									
									<a href="#" class="button sm">More</a>
									
								</div><!-- .dd-left-item -->
								
								<div class="dd-left-item">
									<?php include('i-nav-craft-council.php'); ?>
								</div><!-- .dd-left-item -->
								
							</div><!-- .dd-meta-left -->
							
							<div class="dd-main">
								<div class="dd-menu">
								
									<a href="#" class="dd-menu-item">
									
										<time class="date-block" datetime="2015-10-18">
											<span class="dow">Thursday</span>
											<span class="date">18</span>
											<span class="month">October</span>
										</time><!-- .date-block -->
										<span class="hex-separator"><span></span></span>
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<time class="date-block" datetime="2015-10-19">
											<span class="dow">Friday</span>
											<span class="date">19</span>
											<span class="month">October</span>
										</time><!-- .date-block -->
										<span class="hex-separator"><span></span></span>
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<time class="date-block" datetime="2015-10-20">
											<span class="dow">Saturday</span>
											<span class="date">20</span>
											<span class="month">October</span>
										</time><!-- .date-block -->
										<span class="hex-separator"><span></span></span>
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<time class="date-block" datetime="2015-10-21">
											<span class="dow">Sunday</span>
											<span class="date">21</span>
											<span class="month">October</span>
										</time><!-- .date-block -->
										<span class="hex-separator"><span></span></span>
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<time class="date-block" datetime="2015-10-22">
											<span class="dow">Monday</span>
											<span class="date">22</span>
											<span class="month">October</span>
										</time><!-- .date-block -->
										<span class="hex-separator"><span></span></span>
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->
									
									<div class="dd-contact">
										<?php include('i-nav-contact.php'); ?>
									</div><!-- .dd-menu-item -->
									
									<div class="dd-menu-meta">
										
										<div class="dd-menu-meta-content">
											
											<span class="dd-menu-title">Did You Know?</span>
											
											<p>
												Sed dictum sem ac hendrerit elementum.
												Maecenas aliquet ante id tortor bibendum
												egestas. In eu consectetur augue, ut rutrum
												dolor. Fusce non sagittis ipsum. Integer vel
												vehicula sapien, sed dapibus eros. Ut id massa
												lacinia, vestibulum urna quis
											</p>
											
										</div>									
									</div><!-- .dd-menu-meta -->
									
								</div><!-- .dd-menu -->
							</div><!-- .dd-main -->
							
						</div><!-- .dd -->
					</div><!-- .dd-wrap -->
				</li>
				<li>
					<a href="#">Events</a>
					
					<div class="dd-wrap">
						<div class="dd">
						
							<div class="dd-meta-left dark-bg">
								
								<div class="dd-left-item">
									
									<span class="dd-left-item-title">Events</span>
									<p>
										Claritas est etiam processus dynamicus, qui
										sequitur mutationem consuetudium
										lectorum. Claritas est etiam processus
										dynamicus, qui sequitur mutationem
										consuetudium lectorum.
									</p>
									
									<a href="#" class="button sm">More</a>
									
								</div><!-- .dd-left-item -->
								
								<div class="dd-left-item">
									<?php include('i-nav-craft-council.php'); ?>
								</div><!-- .dd-left-item -->
								
							</div><!-- .dd-meta-left -->
							
							<div class="dd-main">
								<div class="dd-menu">

									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Exhibitions</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Artist Talks</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Industry Day</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Evening Entertainment</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Tours</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<a href="#" class="dd-menu-item">
									
										<span class="dd-menu-title">Markets, Pop Ups, Swap</span>
										
										<span class="hex-separator">
											<span>&nbsp;</span>
										</span>
										
										<p>
											Claritas est etiam processus
											dynamicus, qui sequitur mutationem
											consuetudium lectorum.
										</p>
									
										<span class="button sm blue">More</span>
									</a><!-- .dd-menu-item -->

									<div class="dd-contact">
										<?php include('i-nav-contact.php'); ?>
									</div><!-- .dd-menu-item -->
									
									<div class="dd-menu-meta">
										
										<div class="dd-menu-meta-content">
											
											<span class="dd-menu-title">Did You Know?</span>
											
											<p>
												Sed dictum sem ac hendrerit elementum.
												Maecenas aliquet ante id tortor
											</p>
											
										</div>									
									</div><!-- .dd-menu-meta -->
									
								</div><!-- .dd-menu -->
							</div><!-- .dd-main -->
							
						</div><!-- .dd -->
					</div><!-- .dd-wrap -->
				</li>
				<li><a href="#" class="highlight">Register</a></li>
			</ul>
		</nav>
	
		<div class="nav-top">
			<button class="t-fa-abs fa-search toggle-search">Search</button>
			<a href="#">Main Site</a>
			<a href="#">Become a Sponsor</a>
			<a href="#">News</a>
			<a href="#">Talk to Us</a>
		</div><!-- .nav-top -->
		
	</div><!-- .sw -->
	
	<div class="nav-dd-section-wrap">
		<div class="nav-dd-section">
		</div><!-- .nav-dd-section -->
	</div><!-- .nav-dd-section-wrap -->
	
</div><!-- .nav -->