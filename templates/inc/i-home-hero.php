<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-1.jpg">
		
			<a href="#" class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">The Versatility of Fibre</span>
						<span class="subtitle">
							<span>5 Days &bull; 22 Workshops &bull; 1 Unique Location</span>
						</span><!-- .subtitle -->
						
						<p>
							Wednesday 14 &mdash; Sunday 18 October 2015 <br />
							Gros Morne National Park, a UNESCO World Heritage Site <br />
							Newfoundland &amp; Labrador, Canada
						</p>
						<span class="button big blue">Register</span>	
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</a><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->