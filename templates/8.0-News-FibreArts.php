<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-7.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">News</span>
						<span class="subtitle">
							<span>Sed dictu Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.m sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>

		<div class="filter-section">
			<div class="filter-bar">
				<div class="sw">

					<div class="filter-bar-left">
					
						<div class="count">
							<span class="num">
								22
							</span>
							Articles Found
						</div><!-- .count -->
						
					</div><!-- .filter-bar-left -->

					<div class="filter-bar-meta">
					
						<form action="/" method="post" class="search-form single-form">
							<div class="fieldset">
								<input type="text" name="s" placeholder="Search news...">
								<button class="t-fa-abs fa-search">Search News</button>
							</div><!-- .fieldset -->
						</form>
						
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div><!-- .filter-bar-meta -->
					
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
			
				<div class="sw">
				
					<div class="grid ov-blocks">
						<div class="col">
							<div class="item">
							
								<div class="date-block ib">
									<span class="dow">Wednesday</span>
									<span class="date">14</span>
									<span class="month">October</span>
								</div><!-- .date-block -->
							
								<span class="ov-blocks-title">News Title</span>
								
								<span class="hex-separator">
									<span>&nbsp;</span>
								</span>
								
								<p>
									Claritas est etiam processus
									dynamicus, qui sequitur mutationem
									consuetudium lectorum.
								</p>
							
								<a href="#" class="button blue">More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item">
							
								<div class="date-block ib">
									<span class="dow">Wednesday</span>
									<span class="date">14</span>
									<span class="month">October</span>
								</div><!-- .date-block -->
							
								<span class="ov-blocks-title">News Title</span>
								
								<span class="hex-separator">
									<span>&nbsp;</span>
								</span>
								
								<p>
									Claritas est etiam processus
									dynamicus, qui sequitur mutationem
									consuetudium lectorum.
								</p>
							
								<a href="#" class="button blue">More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item">
							
								<div class="date-block ib">
									<span class="dow">Wednesday</span>
									<span class="date">14</span>
									<span class="month">October</span>
								</div><!-- .date-block -->
							
								<span class="ov-blocks-title">News Title</span>
								
								<span class="hex-separator">
									<span>&nbsp;</span>
								</span>
								
								<p>
									Claritas est etiam processus
									dynamicus, qui sequitur mutationem
									consuetudium lectorum.
								</p>
							
								<a href="#" class="button blue">More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
				
				</div><!-- .sw -->
				
			</div><!-- .filter-content -->
		</div><!-- .filter-section -->
		
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>