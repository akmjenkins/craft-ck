<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-home-hero.php'); ?>

<div class="body">

	<?php include('inc/i-countdown.php'); ?>

	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">

			<?php include('inc/i-instructors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>