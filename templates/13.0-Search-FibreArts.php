<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-7.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-content">
					
						<span class="title">News</span>
						<span class="subtitle">
							<span>Sed dictu Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.m sem ac hendrerit elementum.</span>
						</span><!-- .subtitle -->
						
					</div><!-- .content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-countdown.php'); ?>
	
	<section>
		<div class="sw">
			<div class="paginated-items-container">
				<div class="paginated-items-header">
					
					<span class="title">Search Results for <span class="uc">Fibre Arts</span></span>
					<span class="date">Post Date</span>
					
				</div><!-- .paginated-items-header -->
				
				<div class="paginated-items">
				
					<a class="search-result" href="#">
					
						<div class="lazybg">
							<img src="http://dummyimage.com/150x150/00b0d8/fff" alt="dummy">
						</div>
					
						<div class="search-content">
							<span>Assault on Social Worker and Increased Incidents of Violence Towards Workers Concerning for NAPE President</span>
							<span class="button blue-o">Read More</span>
						</div><!-- .search-content -->

						<time datetime="2014-01-01">3 days ago</time>
					</a><!-- .search-result -->
					
					<a class="search-result" href="#">
					
						<div class="lazybg">
							<img src="http://dummyimage.com/150x150/00b0d8/fff" alt="dummy">
						</div>

						<div class="search-content">								
							<span>NAPE Activists to Gather on Province’s West Coast for Local Officer Training Seminar</span>
							<span class="button blue-o">Read More</span>
						</div><!-- .search-content -->

						<time datetime="2014-01-01">21 days ago</time>
					</a><!-- .search-result -->
					
					<a class="search-result" href="#">
					
						<div class="lazybg">
							<img src="http://dummyimage.com/150x150/00b0d8/fff" alt="dummy">
						</div>
						
						<div class="search-content">
							<span>NAPE Responds to Announcement of New Courthouse Facility; Wants Input in Planning</span>
							<span class="button blue-o">Read More</span>
						</div><!-- .search-content -->

						<time datetime="2014-01-01">31 days ago</time>
					</a><!-- .search-result -->
					
					<a class="search-result" href="#">
					
						<div class="search-content">
							<span>A Labour of Love &mdash; Christian Children’s Fund of Canada</span>
							<span class="button blue-o">Read More</span>
						</div><!-- .search-content -->

						<time datetime="2014-01-01">44 days ago</time>
					</a><!-- .search-result -->
					
					<a class="search-result" href="#">
					
						<div class="lazybg">
							<img src="http://dummyimage.com/150x150/00b0d8/fff" alt="dummy">
						</div>
						
						<div class="search-content">
							<span>UPCOMING SHOP STEWARD SEMINAR – REGION 5</span>
							<span class="button blue-o">Read More</span>
						</div><!-- .search-content -->

						<time datetime="2014-01-01">46 days ago</time>
					</a><!-- .search-result -->
					
					<a class="search-result" href="#">
					
						<div class="lazybg">
							<img src="http://dummyimage.com/150x150/00b0d8/fff" alt="dummy">
						</div>
						
						<div class="search-content">
							<span>NAPE JOB POSTING – Two ERO Positions (NOW CLOSED)</span>
							<span class="button blue-o">Read More</span>
						</div><!-- .search-content -->
						
						<time datetime="2014-01-01">88 days ago</time>
					</a><!-- .search-result -->
					
				</div><!-- .paginated-items -->
				
				<div class="paginated-items-footer">
				
					<div class="arrow-controls">
						<!-- these can also be "a" tags -->
						<button class="prev">Prev</button>
						<button class="next">Next</button>
					</div><!-- .arrow-controls -->
					
					<div class="count">6 of 108</div>
				
				</div><!-- .search-result-footer -->
				
			</div><!-- .paginated-items-container -->
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
			
			<?php include('inc/i-get-in-touch.php'); ?>
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw">
		
			<?php include('inc/i-event-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<?php include('inc/i-sponsors-section.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>