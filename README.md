##MODIFYING THE DOM (e.g. VIA AJAX)

Whenever content is added to the DOM (like in filter sections when the next/previous arrows are clicked), always call `$(document).trigger('updateTemplate')` after the new content/elements have been added to the DOM.

This will cause swipers, custom dropdowns, .ar (aspect ratio) elements, and visible* lazybg elements to be re-built


##GRID - `.grid`

###NOTE
This is actively under development, I tend to clean up/remove/reorganize CSS from time to time as my understanding about `flexbox` improves.
	
Since IE9 has been declared unsupported by JAC, the grid has been redeveloped using a much more robust CSS solution - `flexbox`.
	
###SINGLE CONTAINER
Probably the best part about using `flexbox` is that we no longer have to specify individual rows with markup
e.g. 

    <div class="grid">
      ...first row here
    </div>
    <div class="grid">
      ...second row here
    </div>				
    ...etc...
				
This made it impossible to collapse a row with an odd number of blocks to an even number of blocks (e.g. 3 to 2) at any given breakpoint.
			
With the new flexbox grid, we now specify just one 'grid' element and then all the child elements.
		
    <div class="grid">
      ... child one ...
      ... child two ...
      ... 
      ... child n ...
    </div>
			
**Flexbox ftw**
	
###MARKUP
    <div class="grid">
     <div class="col col-{number}">
      <div class="item">
        <!-- content -->
      </div>
     </div>		
    </div>
		
###COLUMN SIZING
Specified by class on `.col`

Size classes are
			
    .col-1 - 100%
    .col-2 - 50% 2 columns per row
    .col-3 - 33.3333%
    .col-4 - 25%
    .col-5 - 20%
			
    .col-2-3 - 66.6666% (two thirds)
			
    .col-2-5 - 40% (two fifths...)
    .col-3-5 - 60%
    .col-4-5 - 80%
			
#####RESPONSIVE
You can also specify responsive column sizes by using:
    .sm-col-{number} - breakpoint at 750px
    .xs-col-{number} - breakpoint at 500px
				
**All `{number}'s` are same as above**
	
		
###BLOCK SPACING
Specified by class on `.grid`
		
    <div class="grid {spacingclass}">
		
Spacing classes are:
		
    none - default is 20px
    "nopad" - 0px
    "pad10" - 10px
    "pad40" - 40px
		
###EQUAL HEIGHT ROWS
They just work
		
     <div class="grid eqh">
        ...
     </div>
	
Important to note that elements will only be as tall as the tallest element IN THEIR ROW (as determined by the browser)
e.g. An element in the second row will only be as tall as the tallest element in the second row, not the first row, third row, or any other row
			
###VERTICALLY ALIGNING CONTENT
To vertically center content in adjacent blocks - roughly like:

    ---------------------------------------------------------
    |     Some Title            |                           |
    |      subtitle             |      Some Title           |
    |                           |       subtitle            |
    |     some text             |                           |
    |     some text             |       some text           |
    |     some text             |                           |
    |                           |                           |
    ---------------------------------------------------------
`		
just add a class of `vcenter` to `.grid`
			
    <div class="grid eqh vcenter">
       ...
    </div>
			
	
	
##MAIN BODY - `.main-body`

If no sidebar element is present, .content will be full width - i.e. No extra .width-sidebar class is needed on .main-body element
	
		<div class="main-body">
			<div class="content">
				... content ...
			</div>
			
			<aside class="sidebar">
				... sidebar ...
			</aside>
			
		</div>
	

##BLOCKS - `.blocks`

Built on top of a .grid component. All the same rules for spacing/collapsing/vertically aligning/etc apply.
Mainly used with a some content, header image (but not always), and a button (but not always)

    <div class="grid eqh blocks collapse-at-{number}">
     <div class="col col-{number}">
      <div class="block">

        <!-- img-wrap is optional -->
        <div class="img-wrap">
         <div class="img lazybg" data-src="..."></div>
       </div>

       <div class="content">
        ...any content ...
      </div>

     </div><!-- .block -->

     </div><!-- .col -->
    </div><!-- .grid -->
	
###ADD A BUTTON
If the block has a button at the bottom, add a class of .with-button so it looks like this:
	
    <div class="block with-button">
     <div class="content">
      ... any content ...
      <span class="button">...</span>
     </div>
    </div><!-- .block -->
		
This keeps the buttons of all blocks in a row lined up correctly with each other, regardless of the content of each block
	
###LIST VIEW
Specify any number in collapse-at-{number} - i.e. `collapse-at-890` - to collapse the blocks into `list-view` mode at the breakpoint, or leave this class off altogether if you never want `list-view` mode

The major difference between regular blocks and `list-view` mode is the image contained within `.img-wrap` is aligned to the left of the content element

* At less 650px, if in list-view, the img-wrap is removed completely
* You can trigger the blocks to always be in list view by adding a class of `always-list`

##
		
##LAZYYT - `.lazyyt`
Implemented to increase loading time and conserve bandwidth
YouTube videos should be "embedded" this way rather than through <iframe>'s - the <iframe> downloads a lot of content, and runs a lot of JavaScript which
significantly slows down page load, especially if there are multiple videos on a page. By using: 

`<div class="lazyyt" data-youtube-id="" data-ratio="20:13">`

A placeholder is created that looks identical to a YouTube player, but only requires the downloading of the video's image.

Feel free to use whatever aspect ratio you like, `20:13` (or height 65% of width) is a pretty good one, and so is the more popular `16:9` (height is 56.25% of width).
		
##
		
##LAZYBG - `.lazybg`
Created to conserve bandwidth (and provide a fade in effect)
Images will only be downloaded if the element is visible (e.g. no ancestor is set to display:none) and only when this element is just about to be scrolled into view
	
###Two types:
		
####Regular
			
    <div class="lazybg" data-src="..."></div>
				
> use data-src attribute to specify the background image (the width and height of this element must be given via CSS as it's an empty div and will have no width/height by default
> good because it saves bandwidth
				
				
####Rresponsive
    <div class="lazybg with-img">
     <img src="...">
    </div>
				
> this element is used to provide an effect, not conserve bandwidth
> the img element will NEVER BE VISIBLE (so it can be positioned via CSS), the image you see will be as a result of a background image being set on .lazybg with 
    background: center no-repeat; 
    background-size:contain; 
					
Feel free to overwrite the background CSS rules to position the image as required the purpose of the img element is to give the .lazybg container a size (it will wrap it's child img tag height-wise)
			
Any time content is made visible after it (or an ancestor) was initially set to `display:none` - call 

`$(document).trigger('updateTemplate.lazybg')` - or just `$(document).trigger('updateTemplate')`

This will cause lazybg images to download and be displayed (e.g. in tabbed components, every time a tab is changed, this event is triggered)

##

##ASPECT RATIO ELEMENTS - `.ar`

    <div class="ar" data-ar="{number}">
      <div class="ar-child"></div>
    </div>
	
An element that will always have a specific aspect ratio

`.ar-child` will always be 100% of this aspect ratio

e.g. `data-ar="56.25"` - the element will have an aspect ratio of `16:9` `(9/16 = 56.25)`

##
	
##BOUNCE ELEMENTS - `.bounce`
	
Created to DRY out some CSS
Any .bounce element, on hover, will smoothly scale up any .img descendant with an ease-in-out-back timing function