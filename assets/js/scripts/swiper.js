;(function(context) {

	var $document = $(document);
	
	var methods = {
	
		getSwiperWrappers: function() {
			return $('div.swiper-wrapper');
		},
	
		buildSwiper: function(wrapper) {
			
			var 
				settings = {},
				el = $(this),
				swiper = el.children('div.swiper'),
				selectNav = el.find('select.swiper-nav'),
				responsive = swiper.data('responsive');
				
				$.extend(settings,swiper.data());
				if(responsive) {
					settings.responsive = [responsive];
				}
			
			swiper.slick(settings);
			
			selectNav.on('change',function() {
				swiper.slick('goTo',this.selectedIndex);
			});
			
			
		},
		
		updateTemplate: function() {
			methods
				.getSwiperWrappers()
				.filter(function() {
					return typeof $(this).data('slick') === 'undefined'
				})
				.each(function() {
					methods.buildSwiper.apply(this);
				});		
		}
	
	}
	
	$document
		.on('updateTemplate.swiper ready',function() {
			methods.updateTemplate();
		});

}(typeof ns !== 'undefined' ? window[ns] : undefined));