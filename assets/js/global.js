//load all required scripts
;(function(context) {
	
	var tests;
	var preventOverScroll;

	if(context) {
		preventOverScroll = context.preventOverScroll;
		tests = context.tests;
	} else {
		preventOverScroll = require('./preventOverScroll.js');
		tests = require('./tests.js');
	}

	$(document)
		.on('focus','label.reg-item input',function() {
			var el = $(this).closest('label');
			el.addClass('focused');			
		})
		.on('blur','label.reg-item input',function() {
			var el = $(this).closest('label');
			el.removeClass('focused');
		})
		.on('change','label.reg-item input',function() {
			var 
				el = $(this).closest('label'),
				isChecked = $(this).is(':checked');
				
			if(isChecked) {
				el.addClass('selected');
			} else {
				el.removeClass('selected');
			}
			
		})	
		.on('updateTemplate.timer',function() {
			$('div.countdown').countdown();
		})
		.trigger('updateTemplate.timer');
	
	$(document)
		.on('updateTemplate.lazyYT',function() {
			
			$('div.lazyyt')
				.filter(function() {
					return !$(this).hasClass('.lazyYT-container');
				})
				.each(function() { 
					$(this).lazyYT(); 
				});	
				
		}).trigger('updateTemplate.lazyYT');	

	preventOverScroll($('div.nav')[0]);		

	$('.fader').each(function() {

		var 
			
			slickEl,
			el = $(this),
			methods = {
				
				getElementWithSrcData: function(el) {
					return el.data('src') !== undefined ? el : el.find('.bgel').filter(function() { return $(this).data('src') !== undefined });
				},
				
				loadImageForElementAtIndex: function(i) {
					var 
						self = this;
						element = $('.fader-item',el).eq(i),
						sourceElement = this.getElementWithSrcData(element),
						source = sourceElement.data('src');
					
					if(!element.hasClass('loading') && !element.hasClass('loaded')) {
						element.addClass('loading');
						this
							.loadImage(source)
							.then(function() {
								sourceElement
									.add(self.getElementWithSrcData(element.siblings()).filter(function() {
										return $(this).data('src') === source;
									}))
									.css({backgroundImage: 'url('+source+')' })
									.addClass('loaded');
							});
					}
					
				},
				
				loadImage: function(src) {
					var dfd = $.Deferred();
					
					$('<img/>')
						.on('load',function() {
							dfd.resolve();
						})
						.attr('src',src);
				
					return dfd.promise();
				}

			};
			
		el.slick({
			dots:true,
			appendDots:$('.fader-nav',el.parent()),
			appendArrows:$('.fader-controls',el.parent()),
			prevArrow: '<button class="prev"/>',
			nextArrow: '<button class="next"/>',
			draggable:false,
			swipe:true,
			touchMove:true,
			autoplay:true,
			autoplaySpeed: 5000,
			pauseOnHover: false,
			fade:!tests.touch()
		});
		
		el.on('beforeChange',function(slick,e,i) {
			methods.loadImageForElementAtIndex(el.slick('slickCurrentSlide'));
		});
		
		methods.loadImageForElementAtIndex(el.slick('slickCurrentSlide'));

	});
	
		//directions
		(function() {
			
			var methods = {
			
				streetMap: null,
			
				getStreetMap: function() {
					if(!this.streetMap) {
						this.streetMap = $('#street-map');
					}
					
					return this.streetMap;
				},
			
				buildIFrame: function(origin) {
				
					var source = "https://www.google.com/maps/embed/v1/directions?mode=driving";
					source+='&key='+GOOGLE_MAPS_API_KEY;
					source+='&destination='+this.getStreetMap().data('destination');				
					source+='&origin='+origin;
					this.getStreetMap().html('<iframe src="'+source+'" frameborder="0" style="border:0"/>');
				
				},
				
				geolocate: function() {
					var self = this;
					this.getStreetMap().addClass('loading');
					navigator.geolocation.getCurrentPosition(
						function(position) {
							self.onGeolocateDone(position);
						},
						function(positionError) {
							self.onGeolocateFailed(positionError);
						}
					);
				},
				
				onGeolocateDone: function(position) {
					this.buildIFrame(position.coords.latitude+','+position.coords.longitude);
				},
				
				onGelocateFailed: function(positionError) {
					alert(positionError.message);
					this.getStreetMap().removeClass('loading');								
				}
			
			};
			
			$(document)
				.on('submit','#street-form',function() {
					methods.buildIFrame($(this).find('input').val());
					return false;
				})
				.on('click','#directions-geolocate',function() {
					methods.geolocate();
				});
			
		}());

}(typeof ns !== 'undefined' ? window[ns] : undefined));